//
//  AppDelegate.swift
//  ZstdTest
//
//  Created by Dustin Burke on 3/8/22.
//

import UIKit
import GZMZstd

@main
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        GZMZstd.setLoggerDelegate(logger: self)
        
        let zstUrl = Bundle.main.url(forResource: "physicians.json", withExtension: "zst")!
        let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        GZMZstd.decompressAndExtract(zstUrl.path, toPath: documentsUrl.path)
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

extension AppDelegate: GZMZstdLogger {
    func handleLogMessage(_ priority: Int, message: String?) {
        print("\(priority): \(message ?? "")")
    }
}

